import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { NewsHomeComponent } from './components/news-home/news-home.component';


const routes: Routes = [
  {
    path:'home',
    component:NewsHomeComponent,
  },
  {
    path:'**',
    redirectTo:'home',
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
