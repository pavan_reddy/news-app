import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/shared/api.service';

@Component({
  selector: 'app-news-home',
  templateUrl: './news-home.component.html',
  styleUrls: ['./news-home.component.scss']
})
export class NewsHomeComponent implements OnInit {

  newsData;

  constructor(private apiService:ApiService) { }

  ngOnInit() {
    this.apiService.getNewsData().subscribe(
      (data:any) =>{
       this.newsData = data['articles'];
       console.log(this.newsData)
      }
    )
  }

}
