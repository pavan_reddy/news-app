import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(protected http:HttpClient) { }

  getNewsData(){
    return this.http.get(`https://newsapi.org/v2/everything?q=keyword&apiKey=a223b4157157466aa6133b60a211db51`)
    .pipe((response)=> response)
  }
}
